# TortugaAndroid

WebApp for [Tortuga](https://gitlab.com/javier-sedano/tortuga). It consists in a simple WebView embedding the link to the [deployed Tortuga](https://jsedano.duckdns.org/tortuga/).

<a href='https://play.google.com/store/apps/details?id=com.Odroid.tortuga&pcampaignid=pcampaignidMKT-Other-global-all-co-prtnr-py-PartBadge-Mar2515-1'><img alt='Get it on Google Play' src='https://play.google.com/intl/en_us/badges/static/images/badges/en_badge_web_generic.png'/></a>

Master branch:
[![pipeline status](https://gitlab.com/javier-sedano/tortugaandroid/badges/master/pipeline.svg)](https://gitlab.com/javier-sedano/tortugaandroid/commits/master)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=alert_status)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
<details>
<summary>Quality details</summary>

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=bugs)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=code_smells)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=ncloc)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=security_rating)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Technical Debt](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=sqale_index)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.odroid.tortugaandroid&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)

[Sonar analysis](https://sonarcloud.io/dashboard?id=com.odroid.tortugaandroid)
</details>

Keywords: Tortuga, Android, WebView, CI, CD, Sonar, Sonarqube

TODO: 

* Code coverage. Tests are coded and run in the development environment, but test in CI are only Unit Tests, which does not properly cover 100% of the code. Also, coverage is not sent to Sonarqube.

Links:

* https://github.com/Triple-T/gradle-play-publisher
* http://d.android.com/tools/testing
