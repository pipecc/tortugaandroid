A simple WebView embedding the web application Ballena.

Ballena production site: https://jsedano.duckdns.org/ballena/

Ballena source code: https://gitlab.com/javier.sedano/ballena/

This application source code: https://gitlab.com/javier.sedano/tortugaandroid

FOr historical reasons, this application keeps "Tortuga Android" in the name.
