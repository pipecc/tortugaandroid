package com.odroid.tortuga;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.appcompat.app.AppCompatActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.PrepareOnlyThisForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.mockito.PowerMockito.doReturn;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.spy;
import static org.powermock.api.mockito.PowerMockito.when;
import static org.powermock.api.support.membermodification.MemberMatcher.method;
import static org.powermock.api.support.membermodification.MemberMatcher.methods;
import static org.powermock.api.support.membermodification.MemberModifier.replace;
import static org.powermock.api.support.membermodification.MemberModifier.suppress;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(PowerMockRunner.class)
@PrepareOnlyThisForTest({AppCompatActivity.class})
public class MainActivityUnitTest {
    private static final String TORTUGA_URL = "https://jsedano.duckdns.org/ballena/";

    @Test
    public void loadUrlOnCreate() throws Exception {
        MainActivity mainActivitySpy = spy(new MainActivity());
        WebView webViewMock = mock(WebView.class);
        WebSettings webSettingsMock = mock(WebSettings.class);
        mockOnCreate(mainActivitySpy, webViewMock, webSettingsMock);
        Bundle bundleMock = mock(Bundle.class);

        mainActivitySpy.onCreate(bundleMock);

        verify(webSettingsMock, times(1)).setJavaScriptEnabled(true);
        verify(webSettingsMock, times(1)).setDomStorageEnabled(true);
        verify(webViewMock, times(1)).setWebViewClient(any(AppWebClient.class));
        verify(webViewMock, times(1)).loadUrl(TORTUGA_URL);
    }

    private void mockOnCreate(MainActivity mainActivitySpy, WebView webViewMock, WebSettings webSettingsMock) throws NoSuchMethodException {
        suppress(methods(AppCompatActivity.class, "onCreate"));
        doNothing().when(mainActivitySpy).setContentView(R.layout.activity_main);
        doReturn(webViewMock).when(mainActivitySpy).findViewById(R.id.webview);
        when(webViewMock.getSettings()).thenReturn(webSettingsMock);
    }

}
