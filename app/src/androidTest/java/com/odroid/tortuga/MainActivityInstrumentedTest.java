package com.odroid.tortuga;

import android.content.Context;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class MainActivityInstrumentedTest {

    private static final String TORTUGA_URL = "https://jsedano.duckdns.org/ballena/";

    @Rule
    public ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void useAppContext() {
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        assertEquals("com.Odroid.tortuga", appContext.getPackageName());
    }

    @Test
    public void loadsUrlOnStart() {
        MainActivity mainActivity = rule.getActivity();
        final WebView webView = mainActivity.findViewById(R.id.webview);
        assertNotNull(webView);
        webView.post(new Runnable() {
            @Override
            public void run() {
                assertEquals(TORTUGA_URL, webView.getUrl());
                assertTrue(webView.getSettings().getJavaScriptEnabled());
                assertTrue(webView.getSettings().getDomStorageEnabled());
                assertTrue(webView.getWebViewClient() instanceof AppWebClient);
            }
        });
    }

}
